Part of the microfrontend reference architecture described in https://gitlab.com/javier-sedano/ufe5

Read that documentation.

This is an example of a development toolset, that can provide... common linters, common pipeline files, common scripts,...

NOTE: currently it is being published as npm package in npmjs.com... but the only provided content is pipeline files, so mostly useless publication. Just a pattern for further use (scripts, linters,...)
